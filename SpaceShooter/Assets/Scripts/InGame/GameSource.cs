﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSource : ScriptableObjectSinglton<GameSource>
{

    public List<MonoBehaviour> IPullablePrefabs = new List<MonoBehaviour>();

    public List<Bullet> BulletPrefabs = new List<Bullet>();

	public Dictionary<UnitType, Image> bonusIcons = new Dictionary<UnitType, Image>();	

    public Transform SpawnPosition;
    public Transform TargetPosition;
}
