﻿using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviourSinglton<SoundManager>
{
    public List<AudioSource> AudioSources = new List<AudioSource>();

    private List<AudioSource> instantiatedAudioSources = new List<AudioSource>();

    private float soundSpeed = 1;

    public float SoundSpeed
    {
        get
        {
            return soundSpeed;
        }

        set
        {
            soundSpeed = value;
            PlaySoundWithNewSpeed();
        }
    }

    public void PlaySoundWithNewSpeed()
    {
        foreach (AudioSource item in instantiatedAudioSources)
        {
            item.pitch = SoundSpeed;
        }
    }

    public void PlaySound(string clipName, bool loop = false)
    {        
        AudioSource audioSource = instantiatedAudioSources.Find(x => x.clip.name == clipName);

        if (audioSource == null)
        {
            audioSource = AudioSources.Find(x => x.clip.name == clipName);
            if (audioSource != null)
            {
                audioSource = Instantiate(audioSource);
                audioSource.transform.SetParent(transform);
                instantiatedAudioSources.Add(audioSource);
            }
        }
        else
        {
            audioSource.Play();
        }

        audioSource.loop = loop;
        audioSource.pitch = SoundSpeed;
    }
}
