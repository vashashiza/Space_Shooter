﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{ 
    public ScorePanelManager ScorePanel = null;

    [SerializeField]
    private PauseMenuManager pauseMenuPanel = null;
    [SerializeField]
    private GameOverMenuManager gameOverMenuPanel = null;

    [SerializeField]
    private GameObject pauseMenuButton = null;
    [SerializeField]
    private GameObject background = null;

    [SerializeField]
    private float scrollSpeed = 0;
    [SerializeField]
    private float backgroundSizeY = 0;
    private Vector3 backgroundStartPosition;

    private bool isTouch = false;
    public bool IsTouch
    {
        get
        {
            return isTouch;
        }

        set
        {
            if (value != isTouch)
            {
                pauseMenuButton.SetActive(isTouch);
                isTouch = value;
            }
        }
    }

    //Inspector func
    public void ActivePauseMenu()
    {
        pauseMenuPanel.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void ActiveGameOverMenu()
    {
        gameOverMenuPanel.gameObject.SetActive(true);
        Time.timeScale = 0;
    }    

    private void Start()
    {
        backgroundStartPosition = background.transform.position;
    }

    private void Update()
    {
        float newPosition = Mathf.Repeat(Time.time * scrollSpeed, backgroundSizeY);
        background.transform.position = backgroundStartPosition + Vector3.up * newPosition;
    }
}
