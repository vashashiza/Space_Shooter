﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenuManager : GameOverMenuManager
{
    //Inspector func
    public void ResumeGame()
    {
        transform.gameObject.SetActive(false);
        scorePanel.gameObject.SetActive(false);
        Time.timeScale = 1;
    }

    public void OnEnable()
    {
        scorePanel.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ResumeGame();
        }
    }
}
