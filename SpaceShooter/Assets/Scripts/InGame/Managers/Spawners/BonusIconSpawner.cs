﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BonusIconSpawner : MonoBehaviour
{
    public event Action<BonusIcon> OnIconCreate;

    [SerializeField]
    private BonusIcon bonusIconPrefab = null;

    [SerializeField]
    private Transform canvas = null;

    [SerializeField]
    private Sprite moveSpeedIcon = null;
    [SerializeField]
    private Sprite reloadIcon = null;
    [SerializeField]
    private Sprite fireBallIcon = null;
    [SerializeField]
    private Sprite macLeodIcon = null;

    [SerializeField]
    private Vector2 bonusIconStartPosition = new Vector2();

    private float bonusIconXSize = 0;

    private List<BonusIcon> activeIcons = new List<BonusIcon>();

    public void AddBonusIcon(UnitType type, int lifeTime)
    {        
        if (activeIcons.Exists(x => x.Type == type))
        {
            BonusIcon temp = activeIcons.Find(x => x.Type == type);
            temp.LifeTime = lifeTime;
        }
        else
        {
            BonusIcon bonusIcon = Instantiate(bonusIconPrefab, canvas, false);
            bonusIcon.LifeTime = lifeTime;
            bonusIcon.Type = type;
            bonusIcon.OnFinish += BonusIcon_OnFinish;

            riseOnIconCreate(bonusIcon);

            RectTransform rt = bonusIcon.transform.GetComponent<RectTransform>();

            setSprite(type, bonusIcon);

            setPosition(rt);

            activeIcons.Add(bonusIcon);
        }        
    }

    private void BonusIcon_OnFinish(BonusIcon obj)
    {

        if (activeIcons.Count-1 < 0)
        {
            return;
        }

        int i = activeIcons.FindIndex(x => x == obj);
        if (i == activeIcons.Count)
        {
            return;
        }

        activeIcons.Remove(obj);

        while (i < activeIcons.Count)
        {
            float X = bonusIconStartPosition.x + i * bonusIconXSize;
            RectTransform rt= activeIcons[i].transform.GetComponent<RectTransform>();
            rt.localPosition = new Vector3(X, bonusIconStartPosition.y);
            i++;
        }
    }

    private void riseOnIconCreate(BonusIcon bonusIcon)
    {
        if (OnIconCreate != null) OnIconCreate(bonusIcon);
    }

    private void setPosition(RectTransform rt)
    {
        if (activeIcons.Count == 0)
        {
            rt.localPosition = bonusIconStartPosition;
        }
        else
        {
            float X = bonusIconStartPosition.x + activeIcons.Count * bonusIconXSize;
            rt.localPosition = new Vector3(X, bonusIconStartPosition.y);
        }
    }

    private void setSprite(UnitType type, BonusIcon bonusIcon)
    {
        switch (type)
        {
            case UnitType.FireBallBonus:
                bonusIcon.Image.sprite = fireBallIcon;
                break;
            case UnitType.MacLeodBonus:
                bonusIcon.Image.sprite = macLeodIcon;
                break;
            case UnitType.ReloadBonus:
                bonusIcon.Image.sprite = reloadIcon;
                break;
            case UnitType.MoveSpeedBonus:
                bonusIcon.Image.sprite = moveSpeedIcon;
                break;
        }
    }

    private void Start()
    {
        RectTransform rt = bonusIconPrefab.transform.GetComponent<RectTransform>();
        bonusIconXSize = rt.rect.width;
    }
}