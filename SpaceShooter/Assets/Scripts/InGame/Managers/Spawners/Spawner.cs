﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public event Action<int> OnEnemyMurder;

    [SerializeField]
    private BonusIconSpawner bonusIconSpawner = null;

    [SerializeField]
    private float spawnDelay = 1f;
    [SerializeField]
    private int minEnemysInWave = 2;
    [SerializeField]
    private int maxEnemysInWave = 5;

    [SerializeField]
    private float gameDifficult = 1f;
    [SerializeField]
    private float growthGameDifficult = 0.1f;

    [SerializeField]
    private float spawnWavesDelay = 3f;

    [SerializeField]
    private float bonusMaxDelay = 5f;

    private bool isBonus = false;
    public bool IsBonus
    {
        get
        {
            return isBonus;
        }

        set
        {
            isBonus = value;
            if (!isBonus)
            {
                StartCoroutine(spawnBonusRoutine());
            }
        }
    }

    private void Start()
    {
        StartCoroutine(spawnWaves());

        StartCoroutine(spawnBonusRoutine());
    }

    protected IEnumerator spawnBonusRoutine()
    {
        float i = UnityEngine.Random.Range(1f, bonusMaxDelay);
        yield return new WaitForSeconds(i);
        IsBonus = true;
    }

    private IEnumerator spawnWaves()
    {
        while (true)
        {
            int i = UnityEngine.Random.Range(minEnemysInWave, maxEnemysInWave);
            int count = i * (int)gameDifficult;
            StartCoroutine(spawnWithDelay(count));
            yield return new WaitForSeconds(spawnWavesDelay / gameDifficult);
            gameDifficult += growthGameDifficult;
        }
    }

    private IEnumerator spawnWithDelay(int count)
    {
        for (int i = 0; i < count; i++)
        {
            chooseRandomSpawnPosition();
            yield return new WaitForSeconds(spawnDelay);
        }
    }

    private void chooseRandomSpawnPosition()
    {
        Transform tempSP = GameSource.Instance.SpawnPosition;
        Transform tempTP = GameSource.Instance.TargetPosition;

        Vector2 t = new Vector2(Screen.width, 0);
        Vector2 temp = Camera.main.ScreenToWorldPoint(t);
        float xPosition = UnityEngine.Random.Range(-temp.x, temp.x);
        Vector3 startPoint = new Vector3(xPosition, tempSP.position.y, tempSP.position.z);

        float xSecondPosition = UnityEngine.Random.Range(-temp.x, temp.x); 
        Vector3 targetPoint = new Vector3(xSecondPosition, tempTP.position.y, tempTP.position.z);

        Vector3 targetVector = targetPoint - startPoint;
        Vector3 target = Vector3.Normalize(targetVector);

        instantiateRandomEnemyPrefab(startPoint, target);
    }

    private void instantiateRandomEnemyPrefab(Vector3 startPoint, Vector3 targetVector)
    {   
        int index = UnityEngine.Random.Range(Source.EnemyEnumInterval.First, Source.EnemyEnumInterval.Last);
        Enemy enemy = Pull.Instance.Get<Enemy>((UnitType)index);
        enemy.transform.position = startPoint;
        enemy.gameObject.SetActive(true);

        enemy.Target = targetVector;
        enemy.MoveSpeed *= gameDifficult;
        enemy.HitPoints *= gameDifficult;

        enemy.IsBonus = IsBonus;

        if (IsBonus)
        {
            IsBonus = false;
        }

        enemy.OnMurder += Enemy_OnMurder;
    }

    protected virtual void riseOnEnemyMurder(int value)
    {
        if (OnEnemyMurder != null) OnEnemyMurder(value);
    }

    protected virtual void spawnBonus(Vector3 position)
    {
        int index = UnityEngine.Random.Range(Source.BonusEnumInterval.First, Source.BonusEnumInterval.Last + 1);
        Bonus bonus = Pull.Instance.Get<Bonus>((UnitType)index);
        Debug.Log("bonus");
        bonus.transform.position = position;
        bonus.gameObject.SetActive(true);

        bonus.OnActiveBonus += Bonus_OnActiveBonus;
    }

    private void Bonus_OnActiveBonus(Bonus bonus, UnitType type, int lifeTime)
    {
        bonusIconSpawner.AddBonusIcon(type, lifeTime);
        bonus.OnActiveBonus -= Bonus_OnActiveBonus;
    }

    private void Enemy_OnMurder(BaseUnit sender)
    {       
        Enemy enemy = sender as Enemy;

        enemy.OnMurder -= Enemy_OnMurder;

        if (enemy.IsBonus)
        {
            spawnBonus(sender.transform.position);
        }

        riseOnEnemyMurder(enemy.SetPoints);
    }
}
