﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public Player Player = null;

    [SerializeField]
    private ControlPanel controlPanel = null;

    [SerializeField]
    private FirePanel firePanel = null;

    [SerializeField]
    private Spawner spawner = null;

    private SoundManager soundManager;

    [SerializeField]
    private UIController uiController = null;

    [SerializeField]
    private string backgroudSoundName = null;

    private int score = 0;

    private bool isSlowMotion = false;
    public bool IsSlowMotion
    {
        get
        {
            return isSlowMotion;
        }

        set
        {
            if (value!= isSlowMotion)
            {
                if (value)
                {
                    Time.timeScale = 0.5f;
                    soundManager.SoundSpeed = 0.5f;
                }
                else
                {
                    Time.timeScale = 1;
                    soundManager.SoundSpeed = 1;
                }

                isSlowMotion = value;
            }            
        }
    }      

    private void Start()
    {
        soundManager = SoundManager.Instance;
        soundManager.PlaySound(backgroudSoundName, true);

        controlPanel.OnTouch += controlPanel_OnTouch;

        spawner.OnEnemyMurder += Spawner_OnEnemyMurder;

        firePanel.OnTouch += FirePanel_OnTouch;

        uiController.ScorePanel.OnActive += ScorePanel_OnActive;

        Player.OnMurder += Player_OnMurder;       
    }

    private void ScorePanel_OnActive()
    {
        uiController.ScorePanel.Score = score;
    }

    private void Spawner_OnEnemyMurder(int obj)
    {
        score += obj;
    }

    private void FirePanel_OnTouch()
    {
        Player.Shot();
    }

    private void controlPanel_OnTouch(Vector2 touchPosition, bool isTouch)
    {
        setMoveTarget(touchPosition);

        IsSlowMotion = !isTouch;

        uiController.IsTouch = isTouch;
    }

    private void setMoveTarget(Vector2 touchPosition)
    {
        if (Player != null)
        {
            Vector3 playerScreenPoint = Camera.main.WorldToScreenPoint(Player.transform.position);
            playerScreenPoint = new Vector3(touchPosition.x, playerScreenPoint.y, playerScreenPoint.z);
            Player.MoveTo(Camera.main.ScreenToWorldPoint(playerScreenPoint));
        }
    }

    private void Player_OnMurder(BaseUnit sender)
    {
        HighScoreManager.UpdateRatingList(score);

        controlPanel.OnTouch -= controlPanel_OnTouch;
        spawner.OnEnemyMurder -= Spawner_OnEnemyMurder;

        uiController.ActiveGameOverMenu();       
    }
}
