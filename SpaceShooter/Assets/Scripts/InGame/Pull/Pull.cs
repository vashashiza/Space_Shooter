﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pull : MonoBehaviourSinglton<Pull>
{
    private Dictionary<UnitType, List<IPullable>> pullableLists = new Dictionary<UnitType, List<IPullable>>();

    public void Add(IPullable unit)
    {
        unit.Off();

        List<IPullable> list;

        if (!pullableLists.TryGetValue(unit.Type, out list))
        {
            list = new List<IPullable>();
            pullableLists.Add(unit.Type, list);
        }
        list.Add(unit);
    }

    public T Get<T>(UnitType type) where T : MonoBehaviour
    {
        List<IPullable> list;
        if (pullableLists.TryGetValue(type, out list))
        {
            if (list.Count > 0)
            {
                IPullable obj = list[0];
                list.Remove(obj);
                obj.Reset();

                return obj as T;
            }
        }

        MonoBehaviour prefab = GameSource.Instance.IPullablePrefabs.Find((obj) =>
        {
            return obj is T && obj is IPullable && (obj as IPullable).Type == type;
        });

        if (prefab != null)
        {
            T instanse = Instantiate(prefab as T);
            //(instanse as IPullable).Reset();
            return instanse;
        }
        else
        {
            Debug.LogWarning("prefab not found");
        }

        return null;
    }
}
