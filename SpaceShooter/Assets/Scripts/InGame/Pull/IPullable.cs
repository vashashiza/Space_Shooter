﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IPullable
{
    UnitType Type { get; }

    void Off();

    void Reset();
}

public enum UnitType
{
    AsteroidFirst = 0,
    AsteroidSecond = 1,
    AsteroidThird = 2,
    FireBallBonus = 100,
    MacLeodBonus = 101,
    ReloadBonus = 102,
    MoveSpeedBonus = 103,
    Bullet = 200,
    FireBall = 201
}