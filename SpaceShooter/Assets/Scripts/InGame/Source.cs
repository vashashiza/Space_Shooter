﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Source : MonoBehaviour
{
    public static EnumInterval BonusEnumInterval = new EnumInterval(100, 103);
    public static EnumInterval EnemyEnumInterval = new EnumInterval(0, 2);
}

public struct EnumInterval
{
    public int First, Last;

    public EnumInterval(int x, int y)
    {
        First = x;
        Last = y;
    }
}

public abstract class MonoBehaviourSinglton<T> : MonoBehaviour where T : MonoBehaviourSinglton<T>
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<T>();

                if (instance == null)
                {
                    GameObject gameObject = new GameObject(typeof(T).ToString());
                    instance = gameObject.AddComponent<T>();
                }
            }
            return instance;
        }

        protected set
        {
            if (value == null)
            {
                instance = null;
            }
            else
            {
                if (instance == null)
                {
                    instance = value;
                }
                else if (value != instance)
                {
                    Destroy(value);
                }
            }
        }
    }

    protected virtual void Awake()
    {
        Instance = this as T;
    }
}

public abstract class ScriptableObjectSinglton<T> : ScriptableObject where T : ScriptableObjectSinglton<T>
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                string name = typeof(T).ToString();

                instance = Resources.Load<T>(name);

                if (instance == null)
                {
                    instance = CreateAsset(name);
                }
            }
            return instance;
        }
    }

    public static T CreateAsset(string name, string filePath = "Assets/Resources/")
    {
        T scriptableObject = CreateInstance<T>();

#if UNITY_EDITOR
        if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);
        UnityEditor.AssetDatabase.CreateAsset(scriptableObject, string.Format("{0}{1}.asset", filePath, name));
        UnityEditor.AssetDatabase.SaveAssets();
#endif
        return scriptableObject;
    }
}