﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FirePanel : MonoBehaviour, IPointerClickHandler
{
    public event Action OnTouch;

    public void OnPointerClick(PointerEventData eventData)
    {
        riseOnTouch();
    }

    protected virtual void riseOnTouch()
    {
        if (OnTouch != null) OnTouch();
    }
}