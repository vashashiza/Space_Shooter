﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusIcon : MonoBehaviour
{
    public event Action<BonusIcon> OnFinish;

    [SerializeField]
    private Text text = null;

    public Image Image;

    private UnitType type;
    public UnitType Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }

    private int lifeTime = 0;
    public int LifeTime
    {
        get
        {
            return lifeTime;
        }

        set
        {
            lifeTime = value;
        }
    }

    protected virtual void riseOnFinish()
    {
        if (OnFinish != null) OnFinish(this);
    }

    private void Start()
    {
        StartCoroutine(timer());
    }

    private IEnumerator timer()
    {
        while (LifeTime >= 0)
        {
            text.text = LifeTime + "";
            yield return new WaitForSeconds(1f);
            LifeTime -= 1;
        }

        disintegrate();
        riseOnFinish();
    }

    protected virtual void disintegrate()
    {
        if ((object)this != null)
        {
            Destroy(gameObject);
        }
    }


}