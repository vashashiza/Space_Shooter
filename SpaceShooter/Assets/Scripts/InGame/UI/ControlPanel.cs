﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlPanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    public event Action<Vector2, bool> OnTouch;

    public void OnDrag(PointerEventData eventData)
    {
        riseOnTouch(eventData.position, true);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        riseOnTouch(eventData.position, true);       
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        riseOnTouch(eventData.position, false);
    }

    protected virtual void riseOnTouch(Vector2 eventDataPosition, bool isTouch)
    {
        if (OnTouch != null) OnTouch(eventDataPosition, isTouch);
    }
}
