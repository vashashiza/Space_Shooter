﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Unit, ISetDamage, IBeDestroyedAfterRange, IPullable
{
    [NonSerialized]
    public Vector3 Target;

    [NonSerialized]
    public bool IsBonus = false;

    [SerializeField]
    private GameObject explosion = null;
    [SerializeField]
    private string explosionSound = null;

    [SerializeField]
    protected int damage;
    public int Damage
    {
        get
        {
            return damage;
        }
        set
        {
            damage = value;
        }
    }

    [SerializeField]
    protected int range = 200;
    public int Range
    {
        get
        {
            return range;
        }
    }

    [SerializeField]
    protected int setPoints;
    public int SetPoints
    {
        get
        {
            return setPoints;
        }
    }

    protected Vector3 startPosition;
    public Vector3 StartPosition
    {
        get
        {
            return startPosition;
        }

        set
        {
            startPosition = value;
        }
    }

    [SerializeField]
    private UnitType type;
    public UnitType Type
    {
        get
        {
            return type;
        }
    }

    public void Off()
    {
        gameObject.SetActive(false);
    }

    public void Reset()
    {
        IsBonus = false;
        moveSpeed = baseMoveSpeed;
        hitPoints = baseHitPoints;

        isAlive = true; 
    }

    protected override void Awake()
    {
        base.Awake();

        StartPosition = transform.position;
    }

    protected override void move(Vector3 target)
    {
        Vector3 moveSpeedVector3 = target * (moveSpeed * Time.deltaTime);
        transform.position += moveSpeedVector3;
        if (Vector3.Distance(transform.position, StartPosition) >= range)
        {
            disintegrate();
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            if (other.gameObject.GetType() == typeof(FireBall))
            {
                FireBall fireBall = other.gameObject.GetComponent<FireBall>();
                SetDamage(fireBall.GetDamage());
            }
            else
            {
                Bullet bullet = other.gameObject.GetComponent<Bullet>();
                SetDamage(bullet.GetDamage());
            }            
        }
        else if (other.tag == "Player")
        {
            Player player = other.gameObject.GetComponent<Player>();
            player.SetDamage(Damage);
        }
    }

    protected override void death()
    {
        Instantiate(explosion, transform.position, transform.rotation);

        SoundManager.Instance.PlaySound(explosionSound);
        riseOnMurder();

        isAlive = false;

        if ((object)this != null)
        {
            Pull.Instance.Add(this);
        }        
    }

    protected void Update()
    {
        move(Target);
    }
}
