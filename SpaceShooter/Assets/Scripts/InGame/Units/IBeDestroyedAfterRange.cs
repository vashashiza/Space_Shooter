﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IBeDestroyedAfterRange
{
    Vector3 StartPosition { get;}

    int Range { get;}
}