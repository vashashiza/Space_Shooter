﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    protected float baseReloadTime = 0;

    protected bool immortal = false;
    protected bool moveActive = false;   

    protected List<Bullet> bulletPrefabs;

    protected Bullet bulletPrefab;

    protected Vector3 moveTarget;

    protected Dictionary<UnitType, Coroutine> bonusEffects = new Dictionary<UnitType, Coroutine>();
    [SerializeField]//debug
    protected int activeBullet = 0;

    [SerializeField]
    protected float reloadTime = 1f;
    protected bool isReload = true;
    public bool IsReload
    {
        get
        {
            return isReload;
        }

        set
        {
            isReload = value;
            if (!isReload)
            {
                StartCoroutine(reload());
            }
        }
    }

    public override float HitPoints
    {
        get
        {
            return base.hitPoints;
        }

        set
        {
            if (isAlive)
            {               
                if (!immortal || value > base.HitPoints)
                {
                    base.HitPoints = value;

                    if (value < 0)
                    {
                        death();
                    }
                }
            }
        }
    }

    public void MoveTo(Vector3 target)
    {
        moveTarget = target;
        moveActive = true;
    }
    public void UseBonus(Bonus bonus)
    {
        if ((object)this != null)
        {
            Coroutine effectCoroutine;
            if (bonusEffects.TryGetValue(bonus.Type, out effectCoroutine))
            {
                bonusEffects.Remove(bonus.Type);
                StopCoroutine(effectCoroutine);
            }

            switch (bonus.Type)
            {
                case UnitType.MoveSpeedBonus:
                    MoveSpeed += (bonus as IncreaseBonus).BonusValue;
                    effectCoroutine = StartCoroutine(differedAction(bonus.LifeTime, delegate ()
                    {
                        MoveSpeed = baseMoveSpeed;
                    }));
                    break;
                case UnitType.MacLeodBonus:
                    immortal = true;
                    effectCoroutine = StartCoroutine(differedAction(bonus.LifeTime, delegate ()
                    {
                        immortal = false;
                    }));
                    break;
                case UnitType.ReloadBonus:
                    reloadTime = (bonus as IncreaseBonus).BonusValue;
                    effectCoroutine = StartCoroutine(differedAction(bonus.LifeTime, delegate ()
                    {
                        reloadTime = baseReloadTime;
                    }));
                    break;
                case UnitType.FireBallBonus:
                    activeBullet = 1;
                    effectCoroutine = StartCoroutine(differedAction(bonus.LifeTime, delegate ()
                    {
                        activeBullet = 0;
                    }));
                    break;
                default:
                    break;
            }

            bonusEffects.Add(bonus.Type, effectCoroutine);
        }
    }

    public void Shot()
    {
        if (IsReload)
        {
            int index = 200 + activeBullet;
            Bullet bullet = Pull.Instance.Get<Bullet>((UnitType)index);
            bullet.transform.position = transform.position;
            bullet.gameObject.SetActive(true);

            SoundManager.Instance.PlaySound(bullet.ShotSoundName);
            IsReload = false;
        }
    }

    protected IEnumerator differedAction(float delay, Action call)
    {
        yield return new WaitForSeconds(delay);
        call();
    }

    protected IEnumerator reload()
    {
        yield return new WaitForSeconds(reloadTime);
        IsReload = true;
    }

    protected override void Awake()
    {
        base.Awake();
        baseMoveSpeed = moveSpeed;
        baseReloadTime = reloadTime;
        bulletPrefabs = GameSource.Instance.BulletPrefabs;
    }

    protected void Update()
    {
        if (moveActive)
        {
            if (transform.position == moveTarget)
            {
                moveActive = false;
            }
            else
            {
                Vector3 temp = new Vector3(moveTarget.x, transform.position.y, transform.position.z);
                float step = MoveSpeed * Time.deltaTime;

                transform.position = Vector3.MoveTowards(transform.position, temp, step);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shot();            
        }
    }

    protected override void disintegrate()
    {
        Time.timeScale = 0;
        base.disintegrate();
    }
}

