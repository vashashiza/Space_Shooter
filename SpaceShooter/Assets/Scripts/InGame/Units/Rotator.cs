﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public Vector3 RorationStep = new Vector3();

    public void Awake()
    {
        RorationStep = new Vector3(Random.Range(-1, 1f), Random.Range(-1, 1f), Random.Range(-1, 1f));
    }

    public void FixedUpdate()
    {
        transform.Rotate(RorationStep, Time.deltaTime);
    }
}
