﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseUnit : MonoBehaviour
{
    [SerializeField]
    protected float baseMoveSpeed = 0;
    [SerializeField]//debug
    protected float moveSpeed;
    public float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }

        set
        {
            moveSpeed = value;
        }
    }

    protected virtual void Awake()
    {
        moveSpeed = baseMoveSpeed;
    }

    protected virtual void move(Vector3 target)
    {

    }

    protected virtual void disintegrate()
    {
        if ((object)this != null)
        {
            Destroy(gameObject);
        }
    }
}
