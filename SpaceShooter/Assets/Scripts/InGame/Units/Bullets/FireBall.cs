﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : Bullet
{
    [SerializeField]
    protected float attackRadius = 5;
    [SerializeField]
    protected LayerMask layerMask;

    public override float GetDamage()
    {
        AttackArea();
        return 0;
    }

    public void AttackArea()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, attackRadius, layerMask);
        foreach (Collider2D item in colliders)
        {
            Enemy enemy = item.gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {
                float distance = Vector3.Distance(transform.position, enemy.transform.position);
                enemy.SetDamage(damage - distance);
            }
        }
        disintegrate();
    }
}