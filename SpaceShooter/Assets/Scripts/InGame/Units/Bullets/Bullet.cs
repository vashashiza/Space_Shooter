﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : BaseUnit, ISetDamage, IBeDestroyedAfterRange, IPullable
{    
    public string ShotSoundName = "";

    [SerializeField]
    protected int damage;
    public int Damage
    {
        get
        {
            return damage;
        }
    }

    [SerializeField]
    private UnitType type;
    public UnitType Type
    {
        get
        {
            return type;
        }
    }

    [SerializeField]
    protected int range = 200;
    public int Range
    {
        get
        {
            return range;
        }
    }

    protected Vector3 startPosition;
    public Vector3 StartPosition
    {
        get
        {
            return startPosition;
        }

        set
        {
            startPosition = value;
        }
    }

    public virtual float GetDamage()
    {
        disintegrate();
        return damage;
    }

    void IPullable.Off()
    {
        gameObject.SetActive(false);
    }

    void IPullable.Reset()
    {

    }

    protected override void move(Vector3 target)
     {
        Vector3 moveSpeedVector3 = target * (MoveSpeed * Time.deltaTime);
        transform.position += moveSpeedVector3;
        if (Vector3.Distance(transform.position, StartPosition) >= range)
        {
            disintegrate();
        }
    }

    protected override void disintegrate()
    {
        if ((object)this != null)
        {
            Pull.Instance.Add(this);
        }
    }

    protected override void Awake()
    {
        base.Awake();
        StartPosition = transform.position;
    }

    protected void Update()
    {
        move(new Vector3(0, 1, 0));
    }
}