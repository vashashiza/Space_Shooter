﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Unit : BaseUnit
{
    public event Action<Unit> OnMurder;

    protected bool isAlive = true;

    [SerializeField]
    protected float baseHitPoints;
    protected float hitPoints;
    public virtual float HitPoints
    {
        get
        {
            return hitPoints;
        }

        set
        {
            if (isAlive)
            {
                if (value <= 0)
                {
                    death();
                }
                else
                {
                    hitPoints = value;
                }
            }
        }
    }

    public void SetDamage(float damage)
    {
        if (isAlive)
        {
            HitPoints -= damage;
        }        
    }

    protected override void Awake()
    {
        base.Awake();
        hitPoints = baseHitPoints;
    }

    protected virtual void riseOnMurder()
    {
        if (OnMurder != null) OnMurder(this);
    }

    protected virtual void death()
    {        
        isAlive = false;
        riseOnMurder();
        disintegrate();
    }
}
