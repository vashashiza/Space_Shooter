﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : BaseUnit, IBeDestroyedAfterRange, IPullable
{
    public event Action<Bonus, UnitType, int> OnActiveBonus;

    [SerializeField]
    private int lifeTime = 0;
    public virtual int LifeTime
    {
        get
        {
            return lifeTime;
        }
    }

    protected Vector3 startPosition;
    public Vector3 StartPosition
    {
        get
        {
            return startPosition;
        }

        set
        {
            startPosition = value;
        }
    }

    [SerializeField]
    protected int range = 200;
    public int Range
    {
        get
        {
            return range;
        }
    }

    [SerializeField]
    private UnitType type;
    public UnitType Type
    {
        get
        {
            return type;
        }
    }

    protected virtual void riseOnActiveBonus()
    {
        if (OnActiveBonus != null) OnActiveBonus(this, type, lifeTime);
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player player = other.gameObject.GetComponent<Player>();
            player.UseBonus(this);

            riseOnActiveBonus();

            disintegrate();          
        }
    }
    protected override void move(Vector3 target)
    {
        Vector3 moveSpeedVector3 = target * (MoveSpeed * Time.deltaTime);
        transform.position += moveSpeedVector3;
        if (Vector3.Distance(transform.position, StartPosition) >= Range)
        {
            disintegrate();
        }
    }

    protected void Update()
    {
        move(Vector3.down);
    }

    protected override void Awake()
    {
        base.Awake();
        StartPosition = transform.position;
    }

    protected override void disintegrate()
    {
        if ((object)this != null)
        {
            Pull.Instance.Add(this);
        }
    }

    public void Off()
    {
        gameObject.SetActive(false);
    }

    public void Reset()
    {
        
    }
}

