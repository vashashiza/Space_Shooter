﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePanelManager : MonoBehaviour
{
    [NonSerialized]
    public int Score;

    public event Action OnActive;

    [SerializeField]
    private RatingCell bestScore = null;
    [SerializeField]
    private RatingCell currentScore = null;

    private void Start()
    {
        bestScore.Text = HighScoreManager.Rating[0] + "";
    }

    public void OnEnable()
    {
        riseOnActive();
        currentScore.Text = Score + "";
    }

    private void riseOnActive()
    {
        if (OnActive != null) OnActive();
    }
}