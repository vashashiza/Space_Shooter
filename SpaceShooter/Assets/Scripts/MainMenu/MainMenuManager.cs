﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    //Inspector func
    public void ExitGame()
    {        
        Application.Quit();
    }

    //Inspector func
    public void LoadGame()
    {
        SceneManager.LoadScene("GameScene");
    }
}
